(ns com.example.mutations
  "Client-side mutations.
   
   'Server-side' mutations could normally be also defined here, only with
   `#?(:clj ...)` but here even the 'server' runs in the browser so we must
   define them in another ns, which we do in `...pathom`."
  (:require
   [taoensso.timbre :as log]
   [com.fulcrologic.fulcro.mutations :refer [defmutation]]
   [com.fulcrologic.fulcro.routing.dynamic-routing :as dr]
   [com.fulcrologic.fulcro.components :as comp]
   [com.example.tab-router  :as tab-router :refer  [TabRouter]]))

(defmutation create-random-thing [{:keys [tmpid]}]
  (action [{:keys [state] :as env}]
          (swap! state assoc-in [:new-thing tmpid] {:id tmpid, :txt "A new thing!"}))
  (remote [_] true))
(comment
(def id 3)
@state  (doto state

              (swap! assoc-in [:component/id :bigi :data/id]  id)
              (swap! assoc-in [:component/id :bigi :data/aid]  66)

              (swap! assoc-in [:component/id :bigi :data/tabrouter0] "xxx")
              (swap! assoc-in [:component/id :bigi :data/tabrouter] (comp/get-initial-state TabRouter {}))
              ;(swap! assoc-in [:component/id :Datalist] {:xdata/id id})
              ;(swap! assoc-in [:component/id :com.example.detail/pic] {:data/id id})

           #_(swap! assoc-in [:component/id :com.example.detail/txt] {:data/id id}))
           deref
  )
(defmutation set-current-item [{:keys [data/id post-params]}]
  (action [{:keys [state app] :as env}]
          (log/info "set current item:" id)
          (log/info  (get-in @state [:component/id :bigi :data/tabrouter]))
          #_(def state state)
          (doto state
            (swap! assoc-in [:component/id :bigi :local-data/id] [:data/id id])
            (swap! assoc  :current-data/id [:data/id id])
;            (swap! assoc-in [::dr/id ::TabRouter] (comp/get-initial-state TabRouter {}))
            (swap! assoc-in [:component/id :bigi :data/tabrouter]
                   [::dr/id ::tab-router/TabRouter]))
          (dr/target-ready! app post-params))
  (remote [_] false))