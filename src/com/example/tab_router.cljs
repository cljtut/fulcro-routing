(ns com.example.tab-router

(:require
 [com.fulcrologic.fulcro.algorithms.merge :as merge]
 [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
 [com.fulcrologic.fulcro.routing.dynamic-routing :as dr :refer [defrouter]]
 [com.fulcrologic.fulcro.dom :as dom]
 [taoensso.timbre :as log]))

(defsc Pic [this {cur :current-data/id :as props}]
  {:query               [{[:current-data/id '_] [:data/id :data/label :data/pic]}]
   :route-segment       ["pic"]
   ;; TODO reload pic on will-enter. 
   :will-enter          (fn [app params]
                          (log/info "Pic will enter, params:" params)
                          (log/spy (dr/route-immediate [:component/id ::pic])))
   :initial-state       {}
   :ident               (fn [] [:component/id ::pic])}
  (let [{:data/keys [label pic]} cur]
    (dom/div "txt page:" (pr-str pic))))


(defsc Txt [this {cur :current-data/id :as props}]
  {:query               [{[:current-data/id '_] [:data/id :data/label :data/txt]}]
   :route-segment       ["txt"]
   ;; TODO reload text on will-enter. 
   :will-enter          (fn [app params]
   
                          (log/info "Txt will enter, params:" params)
                          (dr/route-immediate [:component/id ::txt]))
   :initial-state       {}
   :ident               (fn [] [:component/id ::txt])}
   (let [{:data/keys [label txt]} cur]
      (dom/div "txt page:" (pr-str txt))))

(defrouter TabRouter [this {:keys [current-state] :as props}]
  {:router-targets [Pic Txt]}
   #_(case current-state
    :initial (dom/div "What to show when the router is on screen but has never been asked to route")
    :pending (dom/div "Loading...")
    :failed (dom/div "Oops"))
  )

(def ui-tab-router (comp/factory TabRouter))
