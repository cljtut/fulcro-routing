(ns com.example.ui
  (:require
;   [nubank.workspaces.card-types.fulcro3 :as ct.fulcro]
;   [nubank.workspaces.core :as ws]
   [com.example.app :refer [SPA]]
   [com.fulcrologic.fulcro.ui-state-machines :as uism]
   [com.fulcrologic.fulcro.algorithms.merge :as merge]
   [com.wsscode.pathom.connect :as pc]
   [com.wsscode.pathom.core :as p]
   [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
   [com.fulcrologic.fulcro.routing.dynamic-routing :as dr :refer [defrouter]]
   [com.fulcrologic.fulcro.dom :as dom]
   [com.fulcrologic.fulcro.networking.mock-server-remote :refer [mock-http-server]]
   [com.fulcrologic.fulcro.mutations :as m]
   [taoensso.timbre :as log]
   [com.fulcrologic.fulcro.algorithms.form-state :as fs]
   [com.fulcrologic.fulcro.algorithms.tx-processing :as txn]
   [com.fulcrologic.fulcro.data-fetch :as df]
   [edn-query-language.core :as eql]
   [clojure.string :as string]
   [com.example.detail :refer [Datalist BigDetail]]
   [com.example.tab-router :refer [TabRouter]]
   [com.fulcrologic.rad.routing :as routing]
   [com.fulcrologic.rad.routing.html5-history :as hist5]
   [com.example.detail :as detail]
   [com.example.tab-router :as tab-router]))

(declare A2)

(defsc A1 [this {:keys [:id] :as props}]
  {:query               [:id]
   :route-segment       ["a1"]
   :will-enter          (fn [app params]
                          (log/info "A1 will enter")
                          (dr/route-deferred [:id "a1"]
                                             (fn [] (js/setTimeout #(dr/target-ready! app [:id "a1"]) 300))))
   :will-leave          (fn [cls props] (log/info "A1 will leave"))
   :allow-route-change? (fn [c] (log/info "A1 allow route change?") true)
   :initial-state       {:id "a1"}
   :ident               :id}
  (let [parent comp/*parent*]
    (dom/div "A1"
             (dom/button
              {:onClick #(routing/route-to! this A2 {})}
              "Go to sibling A2"))))

(defsc A2 [this {:keys [:id] :as props}]
  {:query               [:id]
   :route-segment       ["a2"]
   :will-enter          (fn [app params]
                          (log/info "A2 will enter")
                          (dr/route-deferred [:id "a2"]
                                             (fn [] (js/setTimeout #(dr/target-ready! app [:id "a2"]) 300))))
   :will-leave          (fn [cls props] (log/info "A2 will leave"))
   :allow-route-change? (fn [c] (log/info "A2 allow route change?") true)
   :initial-state       {:id "a2"}
   :ident               :id}
  (dom/div "A2"))

(defsc B1 [this {:keys [:id] :as props}]
  {:query               [:id]
   :route-segment       ["b1"]
   :will-enter          (fn [app params]
                          (log/info "B1 will enter")
                          (dr/route-deferred [:id "b1"]
                                             (fn [] (js/setTimeout #(dr/target-ready! app [:id "b1"]) 300))))
   :will-leave          (fn [cls props] (log/info "B1 will leave"))
   :allow-route-change? (fn [c] (log/info "B1 allow route change?") true)
   :initial-state       {:id "b1"}
   :ident               :id}
  (dom/div "B1"))

(defsc B2 [this {:keys [:id] :as props}]
  {:query               [:id]
   :route-segment       ["b2"]
   :will-enter          (fn [app params]
                          (log/info "B2 will enter")
                          (dr/route-deferred [:id "b2"]
                                             (fn [] (js/setTimeout #(dr/target-ready! app [:id "b2"]) 300))))
   :will-leave          (fn [cls props] (log/info "B2 will leave"))
   :allow-route-change? (fn [c] (log/info "B2 allow route change?") true)
   :initial-state       {:id "b2"}
   :ident               :id}
  (dom/div "B2"))

(defrouter ARouter [this props]
  {:router-targets [A1 A2]})
(def ui-a-router (comp/factory ARouter))

(defrouter BRouter [this props]
  {:router-targets [B1 B2]})
(def ui-b-router (comp/factory BRouter))

(defsc B [this {:keys [id router] :as props}]
  {:query               [:id {:router (comp/get-query BRouter)}]
   :ident               :id
   :route-segment       ["b"]
   :will-enter          (fn [app params]
                          (log/info "B will enter")
                          (dr/route-immediate [:id "b"]))
   :will-leave          (fn [cls props] (log/info "B will leave"))
   :allow-route-change? (fn [c] (log/info "B allow route change?") true)
   :initial-state       {:id "b" :router {}}}
  (dom/div {}
           (dom/h2 "B")
           (dom/button {:onClick (fn [] (routing/route-to! this B1 {}))} "B1 (relative)")
           (dom/button {:onClick (fn [] (routing/route-to! this B2 {}))} "B2 (relative)")
           (ui-b-router router)))

(def ui-b (comp/factory B {:keyfn :id}))

(defsc A [this {:keys [id router] :as props}]
  {:query               [:id {:router (comp/get-query ARouter)}]
   :route-segment       ["a"]
   :will-enter          (fn [app params]
                          (log/info "A will enter")
                          (dr/route-immediate [:id "a"]))
   :will-leave          (fn [cls props] (log/info "A will leave"))
   :allow-route-change? (fn [c] (log/info "A allow route change?") true)
   :initial-state       {:id "a" :router {}}
   :ident               :id}
  (dom/div {}
           (dom/h2 "A")
           (dom/button {:onClick (fn [] (routing/route-to! this A1 {}))} "A1 (relative)")
           (dom/button {:onClick (fn [] (routing/route-to! this A2 {}))} "A2 (relative)")
           (ui-a-router router)))

(def ui-a (comp/factory A {:keyfn :id}))

; -------------------------

(defrouter RootRouter [this props]
  {:router-targets [A B Datalist BigDetail]})

(def ui-router (comp/factory RootRouter))


(defsc Root [this {:root/keys [router datalist] :as props}]
  {:query         [{:root/router (comp/get-query RootRouter)}
                   {:root/datalist (comp/get-query Datalist)}
                   {:ui/hack-router (comp/get-query TabRouter)}

                   #_[::uism/asm-id ::RootRouter]]
   :initial-state (fn [_] {:root/router (comp/get-initial-state RootRouter {})
                           :root/datalist (comp/get-initial-state Datalist {})
                           :ui/hack-router (comp/get-initial-state TabRouter {})
                           })}

  (let [current-tab (some-> (dr/current-route this this) first keyword)]
    (dom/div
     (dom/div :.ui {:style {:backgroundColor "#d88" :padding "1em" :marginBottom "3em"}}
              (dom/h1 :.ui.item.header.container {:style {:color "white"}}  
                "Example routing app: /" (string/join "/" (dr/current-route SPA))
                " Hist5: " (pr-str (hist5/url->route))))
     (dom/div :.ui.container
              (dom/div :.ui.secondary.pointing.menu
                       (dom/a :.item {:classes [(when (= :a current-tab) "active")]
                                      :onClick (fn [] (routing/route-to! this A {}))} "A")
                       (dom/a :.item {:classes [(when (= :b current-tab) "active")]
                                      :onClick (fn [] (routing/route-to! this B {}))} "B")
                       (dom/a :.item {:classes [(when (#{:c :d} current-tab) "active")]
                                      :onClick (fn [] (routing/route-to! this detail/Datalist {}))} "Datalist"))

              ; skip router rendering until ready; ** no visible effect **
              #_(let [top-router-state (or (uism/get-active-state this ::RootRouter) :initial)]
                  (if (= :initial top-router-state)
                    (dom/div :.loading "Loading***")
                    (ui-router router)))
              (ui-router router)
              (dom/div :.ui.segment
                       (dom/h2 "Global navigation")
                       (dom/button {:onClick (fn [] (routing/route-to! this A1 {}))} "A1 ")
                       (dom/button {:onClick (fn [] (routing/route-to! this B2 {}))} "B2 ")
                       (dom/button {:onClick (fn [] (routing/route-to! this A {}))} "A")
                       (dom/button {:onClick (fn [] (routing/route-to! this B {}))} "B")
                       (dom/button {:onClick (fn [] (routing/route-to! this Datalist {}))} "C")
                       (dom/button {:onClick (fn [] (routing/route-to! this detail/BigDetail {:data/id "2"}))} "D")
                       (dom/button {:onClick (fn [] (routing/route-to! this tab-router/Pic {:data/id "2"}))} "D .pic")
                       (dom/button {:onClick (fn [] (routing/route-to! this tab-router/Txt {:data/id "2"}))} "D .txt"))))))

(comment
       (dr/change-route! SPA ["a" "a1"])
  
  (comp/get-initial-state Root)
  (dr/target-ready 1)
  (comp/component-options TabRouter)
  (app/schedule-render! SPA))