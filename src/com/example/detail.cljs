(ns com.example.detail

(:require
;   [nubank.workspaces.card-types.fulcro3 :as ct.fulcro]
;   [nubank.workspaces.core :as ws]
   [com.fulcrologic.fulcro.ui-state-machines :as uism]
   [com.example.app :refer [SPA]]
   [com.fulcrologic.fulcro.algorithms.merge :as merge]
   [com.wsscode.pathom.connect :as pc]
   [com.wsscode.pathom.core :as p]
   [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
   [com.fulcrologic.fulcro.routing.dynamic-routing :as dr :refer [defrouter]]
   [com.fulcrologic.fulcro.dom :as dom]
   [com.fulcrologic.fulcro.networking.mock-server-remote :refer [mock-http-server]]
   [com.fulcrologic.fulcro.mutations :as m]
   [taoensso.timbre :as log]
   [com.fulcrologic.fulcro.algorithms.form-state :as fs]
   [com.fulcrologic.fulcro.algorithms.tx-processing :as txn]
   [com.fulcrologic.fulcro.data-fetch :as df]
   [edn-query-language.core :as eql]
   [clojure.string :as string]
   [com.example.mutations :refer [set-current-item]]
   [goog.object :as gobj]
   [com.example.tab-router :refer [Pic Txt TabRouter ui-tab-router]]
   [com.fulcrologic.rad.routing :as routing]
   [com.fulcrologic.rad.routing.html5-history :as hist5])
  )

(defsc Data [this {:data/keys [id label pic txt] :as props}]
  {:query         [:data/id :data/label :data/pic :data/txt]
   :ident         :data/id}
  (dom/li
   (dom/a {:style {:cursor "pointer"} 
           :onClick (fn [] (routing/route-to! this Pic {:data/id id}))} label)))

(def ui-data (comp/factory Data {:keyfn :data/id}))

(defsc Datalist [this {:list/keys [data]  :as props}]
  {:initial-state {}
   :query         [{:list/data (comp/get-query Data)}
                   ]
   :will-enter          (fn [app params]
                          (log/info "Datalist will enter" params)
                          (dr/route-immediate [:component/id :Datalist]))
   :route-segment ["c"]
   ;:ident        :list/id}
   :ident               (fn [] [:component/id :Datalist])}
  (dom/div
   #_(dom/div "datalist list/id" (str props))
   (map ui-data data)))

(comment
(comp/get-query Datalist)
  )
(def ui-datalist (comp/factory Datalist))

(defn esc-route-to-c [event]
  (log/spy event)
  (when (= (.-key event) 27)
    (routing/route-to! SPA Datalist {})))

(defsc BigDetail [this {:data/keys [tabrouter] :as props cur :current-data/id}]
  {:query               [:current-data/id
                         :local-data/id
                         {[:current-data/id '_] [:data/id :data/label]}
                         {:data/tabrouter (comp/get-query TabRouter)}
                         #_[::uism/asm-id ::TabRouter]]
   :route-segment       ["d" :data/id]
   :will-enter
   (fn [app {:data/keys [id] :as params}]
     (let [iid (js/parseInt id)
           target-ident [:component/id :bigi]]
       (log/info "BigDetail will enter, params:" params)
       (dr/route-deferred target-ident
                            #(comp/transact! app [(set-current-item {:data/id iid :post-params target-ident})]))

       #_(dr/route-immediate [:data/id iid])
       #_(dr/route-immediate target-ident)))
       ;(comp/transact! app [(set-current-item {:data/id iid})])
   :initial-state       (fn [_] {:data/tabrouter (comp/get-initial-state TabRouter {})})
   ;::initial-state (fn [_] {:xi/router (comp/get-initial-state TabRouter {}) })
   ;:ident               :data/id
   :ident               (fn [] [:component/id :bigi])

   ;; TODO not working
   ;; I do NOT undestand what is the exact goal of the code
   :pre-merge (fn [{loaded-data-tree :data-tree, current-state-map :state-map}] ; 
                (def loaded-data-tree loaded-data-tree)
                (def current-state-map current-state-map)
                (log/info "premerge.data", loaded-data-tree)
                (log/info "premerge.state", current-state-map)
                (merge
                 {:data/tabrouter (comp/get-initial-state TabRouter)} ; 
                 #_{:data/tabrouter (get-in current-state-map (comp/get-ident TabRouter {}))} ; 
                 loaded-data-tree))

   ;; ------------ TODO: ADD ESC handler
   :componentDidMount
   (fn [this]
     ;
     (def -props- (comp/props this))
     ;(gobj/set this "keydown" esc-route-to-c)
     #_(.addEventListener js/document "keydown" esc-route-to-c))

   :componentWillUnmount
   (fn [this]
     #_(.removeEventListener js/document "keydown" esc-route-to-c))
   ;
   }
  
   (let [current-tab (some-> (hist5/url->route) :route last keyword)
         {:data/keys [id label]} cur]
     (dom/div
      (dom/div {:style {:border "1px solid black", :boxShadow "6px 6px"}}
               (dom/div :.ui.items.divided {:style {:backgroundColor "#88f" :color "white"}}
                        (dom/div :.item {:style {:backgroundColor "#88f" :color "white"}}
                                 (dom/h2 {:style {:padding ".4em"}} label)
                                 (dom/div :.extra {:style {:color "white" :margin 0 :cursor "pointer"}}
                                          (dom/i :.large.icon.window.close.right.floated
                                                 {:style {:margin 0}
                                                  :onClick (fn [] (routing/route-to! this Datalist {}))}))))
      (dom/div :.ui.grid
               (dom/div :.column.four.wide
                        #_(dom/div (pr-str "detail:" id " Label: " label "; " current-tab "cur-id " cur))
                        (dom/div :.ui.pointing.menu.vertical
                                 (dom/a :.item {:classes [(when (= :pic current-tab) "active")]
                                                :onClick (fn [] (routing/route-to! this Pic {:data/id id}))} "Pic")
                                 (dom/a :.item {:classes [(when (= :txt current-tab) "active")]
                                                :onClick (fn [] (routing/route-to! this Txt {:data/id id}))} "Txt")))
               (dom/div :.column.twelve.wide
                        (ui-tab-router (comp/computed tabrouter {:data/id id}))))))))