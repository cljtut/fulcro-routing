(ns com.example.client
  (:require
   [com.fulcrologic.fulcro.data-fetch :as df]
   [com.example.app :refer [SPA]]
   [com.example.ui :as ui]
   [com.example.detail :as detail]
   [com.example.detail :refer [Data Datalist BigDetail]]
   [com.fulcrologic.fulcro.application :as app]
   [com.fulcrologic.fulcro.components :as comp]
   [com.fulcrologic.fulcro.routing.dynamic-routing :as dr]
   [com.fulcrologic.fulcro.algorithms.merge :as merge]
   [com.fulcrologic.rad.routing.html5-history :as hist5 :refer [html5-history]]
   [com.fulcrologic.rad.routing.history :as history]
   [com.fulcrologic.rad.routing :as routing]))

(defn ^:export init
  "Called by shadow-cljs upon initialization, see shadow-cljs.edn"
  []
  (println "Initializing the app...")
  (app/set-root! SPA ui/Root {:initialize-state? true})
  (dr/initialize! SPA) ; make ready, if you want to use dynamic routing...
  (history/install-route-history! SPA (html5-history))
  (routing/route-to! SPA detail/Datalist {})
  (app/mount! SPA
              (app/root-class SPA)
              "app"
              {:initialize-state? false})
  (merge/merge-component! SPA Data  [{:data/id 1 :data/label "train" :data/pic "<train/>" :data/txt "train text"}
                                         {:data/id 2 :data/label "boat" :data/pic "<boat/>" :data/txt "boat text"}
                                         {:data/id 3 :data/label "plain" :data/pic "<plain/>" :data/txt "plain text"}
                                         {:data/id 5 :data/label "Xtrain" :data/pic "<Xtrain/>" :data/txt "Xtrain text"}
                                         {:data/id 6 :data/label "Xboat" :data/pic "<Xboat/>" :data/txt "Xboat text"}
                                         {:data/id 7 :data/label "Xplain" :data/pic "<Xplain/>" :data/txt "Xplain text"}]

                          :replace [:component/id :Datalist :list/data])
  #_(merge/merge-component! SPA Data [{:data/id 5 :data/label "Xtrain" :data/pic "<Xtrain/>" :data/txt "Xtrain text"}
                                    {:data/id 6 :data/label "Xboat" :data/pic "<Xboat/>" :data/txt "Xboat text"}
                                    {:data/id 7 :data/label "Xplain" :data/pic "<Xplain/>" :data/txt "Xplain text"}]
                          :append [:component/id :Datalist :list/data]
                             ;
                          ))

(comment
  
  
  )

(defn ^:export refresh 
  "Called by shadow-cljs upon hot code reload, see shadow-cljs.edn"
  []
  (println "Refreshing after a hot code reload...")
  (comp/refresh-dynamic-queries! SPA)
  (app/mount! SPA (app/root-class SPA) "app"))

